package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Pattern;

public class Calculator {

    private final static String START = "^[-(]?\\d+([.]\\d+)?";
    private final static String MIDDLE = "([-+/*]([(])?\\d+([.]\\d+)?([)])?)*";
    private final static String END = "(\\d+([.]\\d+))?$";

    private final static String FULL = START.concat(MIDDLE).concat(END);

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(final String statement) {
        return isEmpty(statement) ? null : isValid(statement) ? parse(statement) : null;
    }

    private boolean isEmpty(final String statement) {
        return statement == null || statement.isEmpty();
    }

    private boolean isValid(final String statement) {
        return Pattern.compile(FULL).matcher(statement).find();
    }

    private String parse(final String statement) {
        final String result;

        try {
            result = new ScriptEngineManager().getEngineByName("JavaScript").eval(statement).toString();
        } catch (ScriptException e) {
            return null;
        }

        return isNumber(result) ? null : result;
    }

    private boolean isNumber(final String statement) {
        return statement.equalsIgnoreCase("Infinity");
    }
}
