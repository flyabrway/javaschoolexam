package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(final List<Integer> inputNumbers) {
        final Integer height = Optional.of(inputNumbers)
                .filter(PyramidBuilder::checkSize)
                .filter(PyramidBuilder::nonNull)
                .map(PyramidBuilder::getHeight)
                .orElseThrow(CannotBuildPyramidException::new);

        return build(height, inputNumbers);
    }

    private static Boolean checkSize(final List<Integer> list) {
        return list.size() < Integer.MAX_VALUE - 1;
    }

    private static Boolean nonNull(final List<Integer> list) {
        return !list.contains(null);
    }

    private static Integer getHeight(final List<Integer> list) {
        int height = 0;
        int counter = 0;
        int numbers = 1;

        while (counter < list.size()) {
            counter = counter + numbers;
            numbers = numbers + 1;
            height = height + 1;
        }

        if (counter != list.size()) {
            throw new CannotBuildPyramidException();
        }

        return height;
    }

    private static int[][] build(int height, final List<Integer> numbers) {
        int[][] pyramid = PyramidBuilder.init(height);
        Collections.sort(numbers);

        int index = 0;

        int row = 1;
        while (row <= height) {

            int pos = height - row;
            int numbersPerRow = row;

            for (int i = 0; i < numbersPerRow; i++) {
                pyramid[row - 1][pos] = numbers.get(index);

                pos = pos + 2;
                index = index + 1;
            }

            row = row + 1;
        }

        return pyramid;
    }

    private static int[][] init(int height) {
        return IntStream.range(0, height)
                .mapToObj(i -> new int[length(height)])
                .toArray(int[][]::new);
    }

    private static int length(int height) {
        return (height << 1) - 1;
    }

}
