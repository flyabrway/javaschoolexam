package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public boolean find(final List x, final List y) {
        Optional.ofNullable(x).orElseThrow(IllegalArgumentException::new);
        Optional.ofNullable(y).orElseThrow(IllegalArgumentException::new);

        final String stringX = (String) x.stream()
                .map(Object::hashCode)
                .map(Object::toString)
                .collect(Collectors.joining(".*", "^.*", ".*$"));

        final String stringY = (String) y.stream()
                .map(Object::hashCode)
                .map(Object::toString)
                .collect(Collectors.joining(""));

        return Pattern.compile(stringX).matcher(stringY).find();
    }
}
